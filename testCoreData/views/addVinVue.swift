//
//  addVinVue.swift
//  testCoreData
//
//  Created by Gilles Ciantar on 15/08/2020.
//

import SwiftUI

struct addVinVue: View {
    @StateObject var myVariables = MyVariables()
    @State var nbB:String = ""
    @State var nomVin:String = ""
    var body: some View {
        Form {
            Section(header: Text("Nombre de bouteille")){
                nbBouteilleTextFieldView(nbB: $nbB)
            }
            Section(header: Text("Nom du vin")) {
                nomVinTextFieldView(nomVin: $nomVin)
            }
        }
        .navigationBarTitle("Ajouter des vins")
        .onDisappear {
            if nbB != "" && nomVin != "" {
                let newVin = mesVins( nbB: Int(nbB)! , nomVin: nomVin )
                myVariables.objectWillChange.send()
                myVariables.listeDesVins.append(newVin)
                         
                for i in 0...myVariables.listeDesVins.count - 1 {
                    print(myVariables.listeDesVins[i])
                }
//                myVariables.saveDataNbBNomVin(nbB: nbB, nomVin: nomVin)
            }
        }
    }
}

//struct addVinVue_Previews: PreviewProvider {
//    static var previews: some View {
//        addVinVue()
//    }
//}

