//
//  ContentView.swift
//  testCoreData
//
//  Created by Gilles Ciantar on 15/08/2020.
//

import SwiftUI

struct mainView: View {
    @StateObject var myVariables = MyVariables()
    @State private var nbBTotal:Int = 0
    var body: some View {
        NavigationView{
            VStack {
                listeDesVinsView(listeDesVins: myVariables.listeDesVins)
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    Text("\(nbBTotal) \(myVariables.textBouteille(nbB: Int(nbBTotal)))")
                        .font(Font.custom("Charter-Bold", size: 30))
                }
            }
            .navigationBarItems(
                //                leading:
                //                    HStack {
                //                        NavigationLink(destination: addVinVue()){Text("Options")
                //                            .font(Font.custom("AppleSDGothicNeo-Bold", size: 18))
                //                            .frame(width: 70, height: 25, alignment: .center)
                //                            .foregroundColor(Color(red: 0.5, green: 0.5, blue: 0.5, opacity: 0.75))
                //                        }
                //                    },
                trailing:
                    HStack {
                        NavigationLink(destination: addVinVue()){
                            Text("+")
                                .font(Font.custom("AppleSDGothicNeo-Bold", size: 40))
                                .frame(width: 25, height: 25, alignment: .center)
                                .foregroundColor(.green)
                        }
                    })
        }
    }
}
//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        mainView()
//    }
//}
