//
//  nomVinTextFieldView.swift
//  testCoreData
//
//  Created by Gilles Ciantar on 15/08/2020.
//

import SwiftUI

struct nomVinTextFieldView: View {
    @ObservedObject var myVariables = MyVariables()
    @Binding var nomVin:String
    var body: some View {
        HStack {
            TextField("Nom vin", text: $nomVin )
            .frame(width: 250, height: 25, alignment: .leading)
        }
    }
}

//struct nomVinTextFieldView_Previews: PreviewProvider {
//    static var previews: some View {
//        nomVinTextFieldView()
//    }
//}
