//
//  listeVinsView.swift
//  testCoreData
//
//  Created by Gilles Ciantar on 16/08/2020.
//

import SwiftUI

struct listeDesVinsView: View {
    let listeDesVins:[mesVins]
    var body: some View {
        Text("Count main --> \(listeDesVins.count)")
        List (listeDesVins) { (liste:mesVins) in
            HStack {
                Text("\(String(liste.nbB))")
                Text("\(liste.nomVin)" )
            }
        }
    }
}

//struct listeVinsView_Previews: PreviewProvider {
//    static var previews: some View {
//        listeVinsView()
//    }
//}
