//
//  nbBouteilleTextFieldView.swift
//  testCoreData
//
//  Created by Gilles Ciantar on 15/08/2020.
//

import SwiftUI
import Combine

struct nbBouteilleTextFieldView: View {
    @StateObject var myVariables = MyVariables()
    @Binding var nbB:String
    var body: some View {
        HStack {
            TextField("Nb bouteille", text:$nbB )
                .frame(width: 100, height: 25, alignment: .leading)
                .keyboardType(.numberPad)
                .onReceive(Just(nbB)) { newValue in
                    let filtered = newValue.filter { "0123456789".contains($0) }
                    if filtered != newValue {
                        self.nbB = filtered
                    }
                }
            Text(myVariables.textBouteille(nbB: Int(nbB)! ))
        }
    }
}
