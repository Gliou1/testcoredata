//
//  variables.swift
//  testCoreData
//
//  Created by Gilles Ciantar on 15/08/2020.
//

import Foundation

class MyVariables : ObservableObject {
    @Published var listeDesVins:[mesVins] = []

    func saveDataNbBNomVin(nbB:String, nomVin:String) {
        let newVin = mesVins( nbB: Int(nbB)! , nomVin: nomVin )
//        objectWillChange.send()
        listeDesVins.append(newVin)
                 
        for i in 0...listeDesVins.count - 1 {
            print(listeDesVins[i])
        }
        
        let myVariables = MyVariables()
        let stringBouteille:String = String( myVariables.textBouteille(nbB: Int(nbB)! ) )
        print("sauvegarde de \(nbB) " + stringBouteille + " " + nomVin + " dans core Data")
        print("Count -->",listeDesVins.count)
    }

    func textBouteille(nbB:Int) -> String {
        switch nbB {
        case 0:
            return " Cave vide"
        case 1:
            return " bouteille"
        default:
            return " bouteilles"
        }
    }
    func addVin() {
        let nouveauVin = mesVins(nbB: 1, nomVin: "Essai")
        listeDesVins.append(nouveauVin)
    }
}

struct mesVins : Identifiable {
    let id = UUID()
    let nbB:Int
    let nomVin:String
}
